/////////////////////////////// INCLUINDO AS BIBLIOTECAS //////////////////////////////

#include <iostream>
#include <string>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

/////////////////////////////// INICIO DA FUNCAO PRINCIPAL //////////////////////////////

int main(int argc, char ** argv) {
	
   int quantidade;			//Variavel que le a quantidade de cadastros a serem incluidos
   cout << "Digite a quantidade de cadastros: " << endl;	
   cin >> quantidade;

   Professor cad_professor[quantidade];		//declara um vetor com a quantidade de cadastros informados para professor
   Aluno cad_aluno[quantidade];			//para alunos
   string nome_temp;				//variavel para armazenamento temporario da informacao tipo string
   float real_temp;					//" " " tipo float
   int inteiro_temp, contador, i, selecao[quantidade], opcao;	//variaveis inteiras

   for (i=0; i<=quantidade;i++){	//seta os valores do vetor selecao como 3(default)
   selecao [i]=3;
   }
   for (i=0; i<quantidade; i++){	//inicia o loop para gravar informacoes
   cout << "Digite a opcao de novo cadastro:\n[1] - Aluno\n[2] - Professor\n[3] - Para concluir" << endl;
   cin >> opcao;
   selecao[i]=opcao;	//altera o valor do vetor selecao
   contador += 1;	//conta a quantidade de cadastros efetivamente efetuados
   switch(opcao){	
	case 1:
	cout << "Nome: ";
	cin >> nome_temp;
	cad_aluno[i].setNome(nome_temp);
	cout << "Matricula: ";
	cin >> nome_temp; 
	cad_aluno[i].setMatricula(nome_temp);
	cout << "Idade: ";
	cin >> inteiro_temp;
	cad_aluno[i].setIdade(inteiro_temp);
	cout << "Sexo: ";
	cin >> nome_temp;
	cad_aluno[i].setSexo(nome_temp);
	cout << "Telefone: ";
	cin >> nome_temp;
	cad_aluno[i].setTelefone(nome_temp);

	cout << "Ira: ";
	cin >> real_temp;
	cad_aluno[i].setIra(real_temp);
	cout << "Semestre: ";
	cin >> inteiro_temp;
     	cad_aluno[i].setSemestre(inteiro_temp);
	cout << "Curso: ";
	cin >> nome_temp;
	cad_aluno[i].setCurso(nome_temp);
	selecao [i]=1;
	break;
	
	case 2:
	cout << "Nome: ";
	cin >> nome_temp;
	cad_professor[i].setNome(nome_temp);
	cout << "Matricula: ";
	cin >> nome_temp;
	cad_professor[i].setMatricula(nome_temp);
	cout << "Idade: ";
	cin >> inteiro_temp;
	cad_professor[i].setIdade(inteiro_temp);
	cout << "Sexo: ";
	cin >> nome_temp;
	cad_professor[i].setSexo(nome_temp);
	cout << "Telefone: ";
	cin >> nome_temp;
	cad_professor[i].setTelefone(nome_temp);

	cout << "Cargo: ";
	cin >> nome_temp;
	cad_professor[i].setCargo(nome_temp);
	cout << "Salario: ";
	cin >> real_temp;
	cad_professor[i].setSalario(real_temp);
	cout << "Sala: ";
	cin >> nome_temp;
	cad_professor[i].setSala(nome_temp);
	selecao [i]=2;
   	break;

	case 3:
	break;

	default:
	cout << "Opcao invalida!" << endl;
	selecao [i]=0;
	break;
	}

	if(selecao [i]==3)	//Caso o valor 3(usado como default) nao seje alterado o loop e finalizado
		break;
   }
   
   for (i=0; i<=contador; i++){		//Imprime os cadastros efetivamente efetuados
	if (selecao [i]==1){
	cout << "\n\nDados do aluno:" << endl;	
	cad_aluno[i].imprimeDadosAluno();
	}

	if (selecao [i]==2){
	cout << "\n\nDados do professor:" << endl;
	cad_professor[i].imprimeDadosProfessor();
	}
   }

	
   return 0;
}

/////////////////////////////////////////// FIM ////////////////////////////////////////////
