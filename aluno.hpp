#ifndef ALUNO_HPP
#define ALUNO_HPP

#include "pessoa.hpp"
#include <string>

class Aluno : public Pessoa {

// Atributos
private:
float ira;
int semestre;
string curso;

// Métodos
public:
Aluno();
~Aluno();
void setIra(float ira);
float getIra();
void setSemestre(int semestre);
int getSemestre();
void setCurso(string curso);
string getCurso();

void imprimeDadosAluno();


};

#endif
