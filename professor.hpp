#ifndef professor_h
#define professor_h
#include <string>
#include "pessoa.hpp"
using namespace std;

class Professor: public Pessoa{
	private:
		string cargo;
		float salario;
		string sala;
	public:
		Professor();
		~Professor();
		void setCargo(string cargo);
		string getCargo();
		void setSalario(float salario);
		float getSalario();
		void setSala(string sala);
		string getSala();

		void imprimeDadosProfessor();
};

#endif
