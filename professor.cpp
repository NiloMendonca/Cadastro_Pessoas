#include "professor.hpp"
#include <string>
#include <iostream>

using namespace std;

Professor::Professor(){}

Professor::~Professor(){}

void Professor::setCargo(string cargo){
	this->cargo = cargo;
}

string Professor::getCargo(){
	return this->cargo;
}

void Professor::setSalario(float salario){
	this->salario = salario;
}

float Professor::getSalario(){
	return this->salario;
}

void Professor::setSala(string sala){
	this-> sala = sala;
}

string Professor::getSala(){
	return this->sala;
}

void Professor::imprimeDadosProfessor(){
	imprimeDados();
	cout << "Cargo: " << getCargo() << endl;
	cout << "Salario: " << getSalario() << endl;
	cout << "Sala: " << getSala() << endl;
}
